<?php 
declare(strict_types=1);

session_start();

require __DIR__.'/vendor/autoload.php';
use Core\Router;
use Model\UserManager;

$currentUser = null;
if (isset($_SESSION['user_id'])) {
    $manager = new UserManager();
    $currentUser = $manager->getUserById($_SESSION['user_id']);
}

$router = new Router();
$router->requireActionFromRoute($_GET['page'] ?? $router->getDefaultRoute());




