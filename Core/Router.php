<?php
declare(strict_types=1);

namespace Core;

use Controller\HomepageController;
use Controller\MovieController;
use Controller\UserController;
use Controller\SecurityController;
use Controller\CriticController;

class Router 
{
    public function getDefaultRoute(): string
    {
        return 'home';
    }
    
    public function requireActionFromRoute(string $route): void
    {
        switch($route) {
            case 'home':
                (new HomepageController)->show();
                break;
            case 'movie':
                (new MovieController)->show();
                break;
            case 'user':
                (new UserController)->show();
                break;
            case 'login':
                (new SecurityController)->login();
                break;
            case 'register':
                (new SecurityController)->register();
                break;
            case 'disconnect':
                (new SecurityController)->disconnect();
                break;
            case 'critic_add':
                (new SecurityController)->disconnect();
                break;
            case 'movie_json':
                (new MovieController)->getJson();
                break;
        }
    }
}
