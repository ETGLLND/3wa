<?php

namespace Core;

abstract class Database
{
    public static $host = "home.3wa.io";
    public static $dbname = "proj-14_egalland_projet_final";
    public static $username = "egalland";
    public static $password = "f3aec813ODk3YWUzMzMwNjY5ZGQ0Y2I5MjFhZjI33bfa505d";
    
    public static function getDatabase(): \PDO
    {
        try {
            return new \PDO(
                "mysql:host=" . self::$host . ";port=3307;dbname=" . self::$dbname,
                self::$username,
	            self::$password
	    );
        } catch (\PDOException $e) {
            die('<strong>Erreur MySQL :</strong><br/>' . $e->getMessage());
        }
    }
}
