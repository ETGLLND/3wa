#!/usr/bin/env bash

# How to launch:
# `sh sasswatch.sh`

# Params
input="assets/scss/style.scss"
output="assets/css/style.css"
style="compressed"

# Formatting utils
BOLD="\033[1m"
BLUE="\033[94m"
FEND="\033[0m"

# Script
echo -e "${BLUE}${BOLD}================"
echo -e " WATCHING SASS!"
echo -e "================${FEND}"
echo -e "\n"

sass --style=${style} --update ${input}:${output} --scss --force
sass --style=${style} --watch ${input}:${output} --scss
