<?php
declare(strict_types=1);

namespace Model;

class Critic
{
    private ?int $id;
    private ?int $note;
    private ?string $title;
    private ?string $content;
    private ?int $userId;
    private ?int $movieId;
    private ?\DateTime $createdAt;
    
    public function __construct($userId, $movieId)
    {
        $this->userId = $userId;
        $this->movieId = $movieId;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): void
    {
        $this->note = $note;
    }
    
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
    
    public function getContent(): ?string
    {
        return $this->content;
    }
    
    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
    
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }
    
    public function getMovieId(): ?int
    {
        return $this->movieId;
    }

    public function setMovieId(?int $movieId): void
    {
        $this->movieId = $movieId;
    }
    
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}