<?php
declare(strict_types=1);

namespace Model;

use Core\Database;

abstract class AbstractManager extends Database
{
    protected \PDO $pdo;
    
    public function __construct()
    {
        $this->pdo = $this->getDatabase();
    }
}