<?php
declare(strict_types=1);

namespace Model;

class Movie
{
    private ?int $id;
    private ?string $title;
    private ?string $picture;
    private ?\DateTime $createdAt;
    private ?string $description;
    private ?\DateTime $duration;
    private ?string $director;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
    
    public function getPicture(): ?string
    {
        return $this->picture;
    }
    
    public function setPicture(?string $picture): void
    {
        $this->picture = $picture;
    }
    
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }
    
    public function getDuration(): ?\DateTime
    {
        return $this->duration;
    }
    
    public function setDuration(?\DateTime $duration): void
    {
        $this->duration = $duration;
    }
    
    public function getDirector(): ?string
    {
        return $this->director;
    }
    
    public function setDirector(?string $director): void
    {
        $this->director = $director;
    }
}