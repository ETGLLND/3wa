<?php
declare(strict_types=1);

namespace Model;

use Model\AbstractManager;

class CriticManager extends AbstractManager
{
    public function getLastCritics(): ?array
    {
        $req = $this->pdo->prepare('
        SELECT 
        critic.user_id, 
        critic.movie_id, 
        critic.id, 
        critic.note, 
        critic.title, 
        critic.content, 
        critic.created_at, 
        user.username,
        user.id AS user_id,
        movie.picture AS movie_picture, 
        movie.id AS movie_id
        FROM critic 
        INNER JOIN user ON critic.user_id = user.id
        INNER JOIN movie ON critic.movie_id = movie.id
        ORDER BY critic.created_at DESC
        ');
        $req->execute();
        $critics = $req->fetchAll();
        
        $criticsArray = [];
        $i = 0;
        if ($critics) {
            foreach($critics as $critic) {
                $criticsArray[$i]['user'] = $critic['username'];
                $criticsArray[$i]['movie_picture'] = $critic['movie_picture'];
                $criticsArray[$i]['movie_id'] = $critic['movie_id'];
                $criticsArray[$i]['user_id'] = $critic['user_id'];
                $criticsArray[$i]['critic'] = $this->mapFromArray($critic);
                $i++;
            }
            return $criticsArray;
        }
        return false;
    }
    
    public function getCriticsFromMovieId(int $id): ?array
    {
        $req = $this->pdo->prepare('
        SELECT critic.user_id, critic.movie_id, critic.id, critic.note, critic.title, critic.content, critic.created_at, user.username, user.id AS user_id
        FROM critic
        INNER JOIN user ON critic.user_id = user.id
        WHERE critic.movie_id = :id
        ORDER BY critic.created_at DESC
        ');
        $req->bindValue(':id', $id, \PDO::PARAM_INT);
        $req->execute();
        $critics = $req->fetchAll();
        
        $criticsArray = [];
        $i = 0;
        if ($critics) {
            foreach($critics as $critic) {
                $criticsArray[$i]['critic'] = $this->mapFromArray($critic);
                $criticsArray[$i]['author'] = $critic['username'];
                $criticsArray[$i]['user_id'] = $critic['user_id'];
                $i++;
            }
            return $criticsArray;
        }
        return false;
    }
    
    public function getAverageNoteById(int $id): ?int
    {
        $req = $this->pdo->prepare('
            SELECT AVG(note) AS note FROM `critic` WHERE movie_id = :id
        ');
        $req->bindValue(':id', $id, \PDO::PARAM_INT);
        $req->execute();
        $average = $req->fetch();
        
        return $average ? intval($average['note']) : false;
    }
    
    public function getNumberOfCriticsById(int $id): int
    {
        $req = $this->pdo->prepare('
            SELECT COUNT(*) AS count FROM `critic` WHERE user_id = :id
        ');
        $req->bindValue(':id', $id, \PDO::PARAM_INT);
        $req->execute();
        $count = $req->fetch();
        
        return intval($count['count']) ?? 0;
    }
    
    public function getCriticsFromUserId(int $id): ?array
    {
        $req = $this->pdo->prepare('
        SELECT critic.user_id, critic.movie_id, critic.id, critic.note, critic.title, critic.content, critic.created_at, movie.id AS movie_id, movie.picture AS movie_picture, movie.title AS movie_title
        FROM critic
        INNER JOIN movie ON critic.movie_id = movie.id
        WHERE critic.user_id = :id
        ORDER BY critic.created_at DESC
        ');
        $req->bindValue(':id', $id, \PDO::PARAM_INT);
        $req->execute();
        $critics = $req->fetchAll();
        
        $criticsArray = [];
        $i = 0;
        if ($critics) {
            foreach($critics as $critic) {
                $criticsArray[$i]['critic'] = $this->mapFromArray($critic);
                $criticsArray[$i]['movie_id'] = $critic['movie_id'];
                $criticsArray[$i]['movie_picture'] = $critic['movie_picture'];
                $criticsArray[$i]['movie_title'] = $critic['movie_title'];
                $i++;
            }
            return $criticsArray;
        }
        return null;
        
    }
    
    public function insertCritic(Critic $critic): void
    {
        $req = $this->pdo->prepare(
            'INSERT INTO critic (note, title, content, user_id, movie_id) VALUES (:note, :title, :content, :user_id, :movie_id)'
        );
        $req->bindValue(':note', $user->getNote(), \PDO::PARAM_INT);
        $req->bindValue(':title', $user->getTitle(), \PDO::PARAM_STR);
        $req->bindValue(':content', $user->getContent(), \PDO::PARAM_STR);
        $req->bindValue(':user_id', $user->getUserId(), \PDO::PARAM_INT);
        $req->bindValue(':movie_id', $user->getMovieId(), \PDO::PARAM_INT);
        $req->execute();
    }
    
    public function mapFromArray(array $data): Critic
    {
        $critic = new Critic((int) $data['user_id'], (int) $data['movie_id']);
        $critic->setId((int) $data['id']);
        $critic->setNote((int) $data['note']);
        if (isset($data['title']) && !empty($data['title'])) {
            $critic->setTitle($data['title']);
        }
        $critic->setContent($data['content']);
        $critic->setCreatedAt(new \DateTime($data['created_at']));
        
        return $critic;
    }
}