<?php
declare(strict_types=1);

namespace Model;

class User
{
    private ?int $id;
    private ?string $username;
    private ?string $mail;
    private ?string $password;
    private ?string $image;
    private ?int $roleId;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
     
    public function getUsername(): ?string
    {
        return $this->username;
    }
    
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }
    
    public function getMail(): ?string
    {
        return $this->mail;
    }
    
    public function setMail(?string $mail): void
    {
        $this->mail = $mail;
    }
    
    public function getPassword(): ?string
    {
        return $this->password;
    }
    
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }
    
    public function getImage(): ?string
    {
        return $this->image;
    }
    
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }
    
    public function getRoleId(): ?int
    {
        return $this->id;
    }

    public function setRoleId(?int $roleId): void
    {
        $this->roleId = $roleId;
    }
    
    public function validate(): bool
    {
        return $this->getUsername() !== null && $this->getPassword() !== null;
    }
}