<?php
declare(strict_types=1);

namespace Model;

use Model\AbstractManager;
use Model\Movie;

class MovieManager extends AbstractManager
{
    public function getMovieFromId(int $id): ?Movie
    {
        $req = $this->pdo->prepare('
        SELECT movie.id, movie.title, movie.director, movie.picture, movie.created_at, movie.description, movie.duration
        FROM movie 
        WHERE movie.id = :id
        ');
        $req->bindValue(':id', $id, \PDO::PARAM_INT);
        $req->execute();
        $movie = $req->fetch();
        
        return $movie ? $this->mapFromArray($movie) : false;
    }
    
    public function mapFromArray(array $data): Movie
    {
        $movie = new Movie();
        $movie->setId((int) $data['id']);
        $movie->setTitle($data['title']);
        $movie->setDirector($data['director']);
        $movie->setPicture($data['picture']);
        $movie->setCreatedAt(new \DateTime($data['created_at']));
        $movie->setDescription($data['description']);
        $movie->setDuration(new \DateTime($data['duration']));
        
        return $movie;
    }
}