<?php
declare(strict_types=1);

namespace Model;

use Model\AbstractManager;
use Model\User;

class UserManager extends AbstractManager
{
    public function getFirstUser(): ?User
    {
        $req = $this->pdo->prepare(
            'SELECT * FROM user WHERE user.id = 1'
        );
        $req->execute();
        $user = $req->fetch();
        
        return $user ? $this->mapFromArray($user) : null;
    }
    
    public function getUserById(int $id): ?User
    {
        $req = $this->pdo->prepare(
            'SELECT * FROM user WHERE user.id = :id'
        );
        $req->bindValue(':id', $id, \PDO::PARAM_INT);
        $req->execute();
        $user = $req->fetch();
        
        return $user ? $this->mapFromArray($user) : null;
    }
    
    public function getUserByMail(string $mail): ?User
    {
        $req = $this->pdo->prepare('
            SELECT user.id, user.username, user.password, user.role_id, user.image, user.mail
            FROM user
            WHERE user.mail = :mail
        ');
        $req->bindValue(':mail', $mail, \PDO::PARAM_STR);
        $req->execute();
        $user = $req->fetch();
        
        if ($user === false || empty($user)) {
            return null;
        }

        return $this->mapFromArray($user);
    }
    
    public function insertUser(User $user): void
    {
        $req = $this->pdo->prepare(
            'INSERT INTO user (username, password, mail) VALUES (:username, :password, :mail)'
        );
        $req->bindValue(':username', $user->getUsername(), \PDO::PARAM_STR);
        $req->bindValue(':password', $user->getPassword(), \PDO::PARAM_STR);
        $req->bindValue(':mail', $user->getMail(), \PDO::PARAM_STR);
        $req->execute();
    }
    
    public function mapFromArray(array $data): User
    {
        $user = new User();
        $user->setId((int) $data['id']);
        $user->setUsername($data['username']);
        $user->setMail($data['mail']);
        $user->setPassword($data['password']);
        $user->setImage($data['image']);
        $user->setRoleId((int) $data['role_id']);
        
        return $user;
    }
}