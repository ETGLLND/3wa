'use strict';

document.addEventListener('DOMContentLoaded', function(){
   const CONNECT = document.getElementById("connect");
   const movies = $(".critic-movie-poster");
   
   if (CONNECT) {
       CONNECT.addEventListener("click", event => {
       event.preventDefault();
       let formElement = createFormElement();
       let positionX = CONNECT.offsetTop;
       let positionY = CONNECT.offsetLeft;
       
       formElement.style.top = positionX + 30 + "px";
       formElement.style.left = positionY + "px";
       document.querySelector("body").appendChild(formElement);
   });
   }
   
  
   movies.on("mouseover", event => {
       let movieId = event.target.dataset.filmid;
       $.getJSON('?page=movie_json&id=' + movieId, getMovie);
   });
   
   movies.on("mouseleave", () => {
       $(".movie_json_info").remove();
   });
});