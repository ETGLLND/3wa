'use strict';

function createFormElement () {
       let newDiv = document.createElement("div");
       
       let newForm = document.createElement("form");
       newForm.setAttribute("method", "post");
       newForm.setAttribute("action", "?page=login");
       
       let labelMail = document.createElement("form");
       labelMail.setAttribute("for", "mail");
       labelMail.textContent = "Email";
       
       let inputMail = document.createElement("input");
       inputMail.setAttribute("type", "email");
       inputMail.setAttribute("name", "mail");
       inputMail.setAttribute("id", "mail");
       
       let labelPassword = document.createElement("form");
       labelPassword.setAttribute("for", "password");
       labelPassword.textContent = "Mot de passe";
       
       let inputPassword = document.createElement("input");
       inputPassword.setAttribute("type", "text");
       inputPassword.setAttribute("name", "password");
       inputPassword.setAttribute("id", "password");
       
       let inputSubmit = document.createElement("input");
       inputSubmit.setAttribute("type", "submit");
       inputSubmit.setAttribute("value", "Valider");
       
       newForm.appendChild(labelMail);
       newForm.appendChild(inputMail);
       newForm.appendChild(labelPassword);
       newForm.appendChild(inputPassword);
       newForm.appendChild(inputSubmit);
       newDiv.appendChild(newForm);
       newDiv.style.position = "absolute";
       newDiv.style.background = "#F5F5F5";
       newDiv.style.padding = "5px";
       
       return newDiv;
   }
   
function getMovie (movie) {
    let newDiv = document.createElement("div");
    let title = document.createElement("p");
    let director = document.createElement("p");
    
    title.textContent = movie.title;
    director.textContent = movie.director;
    
    newDiv.appendChild(title);
    newDiv.appendChild(director);
    
    newDiv.style.position = "absolute";
    newDiv.style.background = "#F5F5F5";
    newDiv.setAttribute("class", "movie_json_info")
    newDiv.style.top = 0;
    newDiv.style.left = 0;
    
    $('body').append(newDiv);
}