<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_10c678ca33ca50d97d7b414022f332136bcca0b6744e021d30da5ad7a22765fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layout.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<h1>Se connecter</h1>
<form method=\"post\">
    <label for=\"username\">Nom d'utilisateur</label>
    <input type=\"text\" id=\"username\" name=\"username\">
    <label for=\"mail\">E-mail</label>
    <input type=\"email\" id=\"mail\" name=\"mail\">
    <label for=\"password1\">Mot de passe</label>
    <input type=\"text\" id=\"password1\" name=\"password1\">
    <label for=\"password2\">Validez votre mot de passe</label>
    <input type=\"text\" id=\"password2\" name=\"password2\">
    <input type=\"submit\" value=\"Valider\">
</form>
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "register.html.twig", "/home/egalland/sites/3wa-projet-final/Views/register.html.twig");
    }
}
