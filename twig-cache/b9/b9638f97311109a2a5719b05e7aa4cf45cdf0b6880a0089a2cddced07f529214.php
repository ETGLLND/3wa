<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* layout.html.twig */
class __TwigTemplate_cc84d94061e0f753a78c215173b15ff882e0a5399c517f6fc2525c294f1c3f9b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr-FR\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"assets/css/style.css\">
    <script src=\"assets/js/script.js\"></script>
    <script src=\"assets/js/functions.js\"></script>
    <title>Critic'Movies - ";
        // line 9
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</title>
</head>
<body>
    <!-- HEADER -->
    <header>
        <div class=\"header-container\">
            <a href=\"index.php\">
                <img class=\"header-logo\" src=\"img/logo.PNG\" alt=\"logo\">
            </a>
            <ul>
                ";
        // line 19
        if (($context["session"] ?? null)) {
            // line 20
            echo "                    <li><a href=\"?page=user&id=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "user_id", [], "any", false, false, false, 20), "html", null, true);
            echo "\">Mon profil</a></li>
                    <li><a href=\"?page=disconnect\">Se déconnecter</a></li>
                ";
        }
        // line 23
        echo "                ";
        if ( !($context["session"] ?? null)) {
            // line 24
            echo "                    <li id=\"connect\"><a href=\"?page=login\">Se connecter</a></li>
                    <li><a href=\"?page=register\">S'inscrire</a></li>
                ";
        }
        // line 27
        echo "            </ul>
        </div>
        <div class=\"header-separator\"></div>
    </header>

    <!-- MAIN -->
    <main>
        <div class=\"main-container\">
    ";
        // line 35
        $this->displayBlock('main', $context, $blocks);
        // line 37
        echo "       </div>
    </main>
   <!-- FOOTER -->
    <footer>
        <p><small>© 2020 Critic'Movies</small></p>
    </footer>
    <script src=\"node_modules/jquery/dist/jquery.min.js\"></script>
</body>
</html>";
    }

    // line 35
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 36,  102 => 35,  90 => 37,  88 => 35,  78 => 27,  73 => 24,  70 => 23,  63 => 20,  61 => 19,  48 => 9,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "layout.html.twig", "/home/egalland/sites/3wa-projet-final/Views/layout.html.twig");
    }
}
