-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 23 juin 2020 à 23:12
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP : 7.3.15-3+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `proj-14_egalland_projet_final`
--

-- --------------------------------------------------------

--
-- Structure de la table `critic`
--

CREATE TABLE `critic` (
  `id` int(11) NOT NULL,
  `note` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_bin NOT NULL,
  `content` text COLLATE utf8mb4_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `critic`
--

INSERT INTO `critic` (`id`, `note`, `title`, `content`, `user_id`, `movie_id`, `created_at`) VALUES
(5, 8, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a pulvinar sapien, in commodo leo. Aenean id nibh bibendum, lobortis purus at, euismod erat. Integer in posuere orci, sit amet sagittis dui. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed dapibus gravida feugiat.', 3, 2, '2020-06-23 23:11:06'),
(6, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a pulvinar sapien, in commodo leo. Aenean id nibh bibendum, lobortis purus at, euismod erat. Integer in posuere orci, sit amet sagittis dui. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed dapibus gravida feugiat.', 3, 1, '2020-06-23 23:11:06');

-- --------------------------------------------------------

--
-- Structure de la table `movie`
--

CREATE TABLE `movie` (
  `id` int(11) NOT NULL,
  `title` varchar(31) COLLATE utf8mb4_bin NOT NULL,
  `director` varchar(79) COLLATE utf8mb4_bin NOT NULL,
  `picture` varchar(39) COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `description` text COLLATE utf8mb4_bin NOT NULL,
  `duration` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `movie`
--

INSERT INTO `movie` (`id`, `title`, `director`, `picture`, `created_at`, `description`, `duration`) VALUES
(1, 'Midsommar', 'Ari Aster', 'midsommar.jpg', '2019-07-31 00:00:00', 'Dani et Christian sont sur le point de se séparer quand la famille de Dani est touchée par une tragédie. Attristé par le deuil de la jeune femme, Christian ne peut se résoudre à la laisser seule et l’emmène avec lui et ses amis à un festival estival qui n’a lieu qu’une fois tous les 90 ans et se déroule dans un village suédois isolé. Mais ce qui commence comme des vacances insouciantes dans un pays où le soleil ne se couche pas va vite prendre une tournure beaucoup plus sinistre et inquiétante.', '02:27:00'),
(2, 'Scarface', 'Brian De Palma', 'scarface.jpg', '1983-11-09 00:00:00', 'Antonio et Manny sont des malfrats expulsés par le régime communiste cubain. Arrivés à Miami, ils se frayent un chemin dans un nouveau monde criminel. ', '02:50:00'),
(3, 'L\'Attaque des tomates tueuses !', 'John De Bello', 'tomates.jpg', '1978-10-08 00:00:00', 'Plusieurs scientifiques s\'unissent pour sauver le monde de tomates tueuses mutantes. Oui oui, vous avez bien lu. ', '01:27:00');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(15) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'standard'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(23) COLLATE utf8mb4_bin NOT NULL,
  `mail` varchar(127) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(127) COLLATE utf8mb4_bin NOT NULL,
  `image` varchar(39) COLLATE utf8mb4_bin NOT NULL DEFAULT 'default.jpg',
  `role_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `mail`, `password`, `image`, `role_id`) VALUES
(3, 'bonjour', 'bonjour@bonjour.com', '$2y$10$HjgUujqxiBt4Dntrj5OxUee0Sf4n0KlfmGxz.XkDqYir/Q2K/Hj4O', 'default.jpg', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `critic`
--
ALTER TABLE `critic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CRITIC_MOVIE_ID` (`movie_id`),
  ADD KEY `CRITIC_USER_ID` (`user_id`);

--
-- Index pour la table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ROLE_USER` (`role_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `critic`
--
ALTER TABLE `critic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `movie`
--
ALTER TABLE `movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `critic`
--
ALTER TABLE `critic`
  ADD CONSTRAINT `CRITIC_MOVIE_ID` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CRITIC_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `ROLE_USER` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
