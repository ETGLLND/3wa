<?php
declare(strict_types=1);

namespace Controller;

use Controller\AbstractController;
use Model\CriticManager;

class HomepageController extends AbstractController
{
    public function show(): void
    {
        $critics = (new CriticManager())->getLastCritics();
        
        echo $this->twig->render('homepage.html.twig', [
            'critics' => $critics,
            'title' => "Notez vos films préférés !"
            ]);
    }
}