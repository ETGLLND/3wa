<?php
declare(strict_types=1);

namespace Controller;

use Controller\AbstractController;
use Model\MovieManager;
use Model\CriticManager;

class MovieController extends AbstractController
{
    public function show(): void
    {   
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $movie = (new MovieManager())->getMovieFromId((int) $_GET['id']);
            $critics = (new CriticManager())->getCriticsFromMovieId((int) $_GET['id']);
            $average = (new CriticManager())->getAverageNoteById((int) $_GET['id']) ?? 0;
            if ($movie && $critics) {
                echo $this->twig->render('movie.html.twig', [
                    'movie' => $movie,
                    'critics' => $critics,
                    'average' => $average,
                    'title' => $movie->getTitle()
                ]);
            } else {
            header('Location: index.php');
            die;
            }
        } else {
            header('Location: index.php');
            die;
        }
    }
    
    public function getJson()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $movie = (new MovieManager())->getMovieFromId((int) $_GET['id']);
            $title = $movie->getTitle();
            $director = $movie->getDirector();
            $description = $movie->getDescription();
            $movieArray = ['title' => $title, 'director' => $director ];
            echo json_encode($movieArray);
        }
    }
}