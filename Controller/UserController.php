<?php
declare(strict_types=1);

namespace Controller;

use Controller\AbstractController;
use Model\UserManager;
use Model\CriticManager;

class UserController extends AbstractController
{
    public function show(): void
    {   
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $user = (new UserManager())->getUserById((int) $_GET['id']);
            $numberCritics = (new CriticManager())->getNumberOfCriticsById((int) $_GET['id']);
            $critics = (new CriticManager())->getCriticsFromUserId((int) $_GET['id']);
            if ($user) {
                echo $this->twig->render('user.html.twig', [
                        'user' => $user,
                        'numberCritics' => $numberCritics,
                        'critics' => $critics,
                        'title' => $user->getUsername()
                ]);
            }
        }
    }
}
