<?php
declare(strict_types=1);

namespace Controller;

use Controller\AbstractController;
use Model\UserManager;
use Model\User;

class SecurityController extends AbstractController
{
    public function login(): void
    {
        if (isset($_POST['mail']) && isset($_POST['password'])) {
            $manager = new UserManager();
            $user = $manager->getUserByMail($_POST['mail']);
            //dump($user); die;
            if ($user instanceof User) {
                if (password_verify($_POST['password'], $user->getPassword())) {
                    $_SESSION['user_id'] = $user->getId();
                    header("Location: index.php");
                    die;
                }
            }
        } 
            echo $this->twig->render('login.html.twig', [
                'title' => "Connexion"
            ]);
    }
    
    public function register(): void
    {
        if (isset($_POST['username'])
            && isset($_POST['mail'])
            && isset($_POST['password1'])
            && isset($_POST['password2'])
            && $_POST['password1'] === $_POST['password2']) {

            $user = new User();
            $user->setUsername($_POST['username']);
            $user->setMail($_POST['mail']);
            $user->setPassword(password_hash($_POST['password1'], PASSWORD_BCRYPT));

            if ($user->validate() === false) {
                die("Invalid user.");
            }

            $manager = new UserManager();
            $manager->insertUser($user);

            header('Location: index.php');
            die;
        }

        echo $this->twig->render('register.html.twig', [
                'title' => "Enregistrement"
            ]);
    }
    
    public function disconnect(): void
    {
        session_destroy();
        $_SESSION = null;
        header('Location: index.php');
    }
}