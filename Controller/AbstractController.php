<?php
declare(strict_types=1);

namespace Controller;

use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;
use Core\Database;

abstract class AbstractController
{
    protected FilesystemLoader $loader;
    protected Environment $twig;
    
    public function __construct()
    {
        $this->loader = new FilesystemLoader('Views');
        $this->twig = new Environment($this->loader, [
            'cache' => 'twig-cache',
            'auto_reload' => true
            ]);
        $this->twig->addGlobal('session', $_SESSION);
    }
}