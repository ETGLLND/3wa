<?php
declare(strict_types=1);

namespace Controller;

use Controller\AbstractController;
use Model\Critic;

class CriticController extends AbstractController
{
    public function add(): void
    {   
        if(isset($_GET['id'])
        && !empty($_GET['id'])
        && isset($_POST['title'])
        && isset($_POST['content'])
        && isset($_POST['note'])) {
            $critic = new Critic((int) $_SESSION['user_id'], (int) $_GET['id']);
            $critic->setTitle($_POST['title']);
            $critic->setContent($_POST['content']);
            $critic->setNote((int) $_POST['note']);
            
            (new CriticManager())->insertCritic($critic);
        } else if (!isset($_GET['id']) || empty($_GET['id'])) {
            header('Location: index.php');
            die;
        } else {
            echo $this->twig->render('add_critic.html.twig', [
                'title' => "Ecrire une critique"
            ]);
        }
        
    }
}